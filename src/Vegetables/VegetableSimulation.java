/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetables;

import java.util.ArrayList;

/**
 *
 * @author levansen
 */
public class VegetableSimulation {
    
    public static void main(String[] args){
    ArrayList<Vegetable> vegetables = new ArrayList<>();
    
    
    Beet beet1 = new Beet("red",3);
    Beet beet2 = new Beet("purple", 1);
    
    Carrot carrot1 = new Carrot("orange", 2);
    Carrot carrot2 = new Carrot("green", 1);
    
    vegetables.add(beet1);
    vegetables.add(beet2);
    vegetables.add(carrot1);
    vegetables.add(carrot2);
    
    for(Vegetable veg : vegetables){
        System.out.println(veg.color);
        System.out.println(veg.size);
        System.out.println(veg.isRipe());
    }
    
  }
 
    
}
